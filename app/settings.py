from pydantic_settings import BaseSettings
from enum import Enum


ENV_PREFIX = 'FIN_CRM_'


class InstructionsTypes(Enum):
    """
    PVK - правила внутреннего контроля.
    CI - целевой инструктаж
    PUZ - повышение уровня знаний
    VI -  вводный инструктаж
    DI - дополнительный инструктаж
    """
    PVK = 1
    CI = 2
    PUZ = 3
    VI = 4
    DI = 5


class AddressTypes(Enum):
    """
    LEGAL - Юридический.
    ACTUAL - Фактический
    MAILING - Почтовый
    """
    LEGAL = 1
    ACTUAL = 2
    MAILING = 3


class Settings(BaseSettings):
    JWT_SECRET_BASE64 :str = 'bXlBdzNzMG0zUzNjcjM3'
    POSTGRES_URL :str = 'postgresql+asyncpg://finconsultcrm:Windowsmustdie1~@188.168.35.10:55432/finconsultcrm'
    POSTGRES_SYNC_URL :str = 'postgresql://finconsultcrm:Windowsmustdie1~@188.168.35.10:55432/finconsultcrm'
    # POSTGRES_SETTINGS_URL = "postgresql+asyncpg://report_service:example@10.101.3.118:5432/report_service?prepared_statement_cache_size=0"
    # POSTGRES_SETTINGS_TEST_URL = "postgresql://report_service:example@10.101.3.118:5432/report_service"
    PORT :int = 4300
    HOST :str = '0.0.0.0'
    ENABLE_AUTORELOAD :bool = True
    ENABLE_AUTHORIZATION :bool = True
    WORKERS :int = 1
    TEMPLATE_FILES_PATH :str = '/var/www/template_files/'

    VERTICA_CONNECTION_TIMEOUT :int = 120

    class Config:
        env_prefix = ENV_PREFIX


settings = Settings()
