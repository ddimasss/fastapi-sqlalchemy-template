from sqlalchemy.orm import Session
from fastapi import Depends
from app.service.sqlalchemy import get_session, get_session_test
from sqlalchemy import select
from ..models.claim import Claim
from ..schemas.claim import ClaimRequest
from sqlalchemy.sql import func
from sqlalchemy.ext.asyncio import (
    AsyncSession
)

async def get_claims(
        claim_request: ClaimRequest,
        db: AsyncSession = Depends(get_session)
):
    print(claim_request)
    result = {}
    select_claims = select(func.count(Claim.claimid), func.sum(Claim.requested_sum).label("requested_sum"), func.sum(Claim.paid_sum).label("paid_sum"))

    #
    ### Group by constructor
    field_objects = []
    for field_name in claim_request.aggregate_by:
        field_objects.append(getattr(Claim, field_name))
    select_claims = select_claims.add_columns(*field_objects)
    select_claims = select_claims.group_by(*field_objects)

    ### Where constructor
    for filter in claim_request.filters:
        column_object = getattr(Claim, filter.name)
        method = getattr(column_object, filter.operation)
        select_claims = select_claims.where(method(filter.values))


    reports = (await db.execute(select_claims)).all()
    result['reports'] = reports

    return result


async def get_claims_test(
        claim_request: ClaimRequest,
        db: Session = Depends(get_session_test)
):
    print(claim_request)
    result = {}
    select_claims = select(func.count(Claim.claimid), func.sum(Claim.requested_sum).label("requested_sum"), func.sum(Claim.paid_sum).label("paid_sum"))

    #
    ### Group by constructor
    field_objects = []
    for field_name in claim_request.aggregate_by:
        field_objects.append(getattr(Claim, field_name))
    select_claims = select_claims.add_columns(*field_objects)
    select_claims = select_claims.group_by(*field_objects)

    ### Where constructor
    for filter in claim_request.filters:
        column_object = getattr(Claim, filter.name)
        method = getattr(column_object, filter.operation)
        select_claims = select_claims.where(method(filter.values))


    reports = db.execute(select_claims).all()
    result['reports'] = reports

    return result
