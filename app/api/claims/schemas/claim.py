from pydantic import BaseModel, Field
from enum import Enum
from datetime import date
from typing import List, Optional, Any

class Role(str, Enum):
    '''Organization role'''
    transportCompany = 'transportCompany'
    insuranceCompany = 'insuranceCompany'
    broker = 'broker'


class GroupBy(str, Enum):
    ''' Группировка по полям
        - 'Тип страхования'
        - 'Предмет обращения'
        - 'Статус претензии'
    '''
    insurance_type = 'insurance_type'  # 'Тип страхования'
    subject = 'subject'  # 'Предмет обращения'
    status = 'status'  # 'Статус претензии'


class Operations(str, Enum):
    in_ = 'in_'
    like = 'like'
    eq = '__eq__'


class Filter(BaseModel):
    name: str
    operation: Operations
    values: Any

    class Config:
        use_enum_values = True


class ClaimRequest(BaseModel):
    role: Role = Field(...)
    aggregate_by:  Optional[List[GroupBy]] = []
    filters: Optional[List[Filter]] = []
    # "mspName": "DellinMSP"
    # "mspNames": ["TheDummyMSP"]
    # "dateRange": []
    #  "insuranceTypes": ["Грузы", "Неизвестно"]
    # "claimStatuses": []
    # "claimSubjects": []
    # "transportCompanies": []
    # "incuranceCompanies": []
    # "currencyCode": "RUB"
    # "aggregateTypes": []
    # "vipStatus": false
    class Config:
        use_enum_values = True

class Claim(BaseModel):
    class Config:
        from_attributes = True
    claimid: int
    claimnumber: str
    insurance_company_msp: str
    insurance_company: str
    transport_company_msp: str
    transport_company: str
    objectnumber: str
    objectdate: date
    insurance_type: str
    requested_sum: float
    paid_sum: float             # = Column(Float)
    status: str                 # Column(String(length=70))
    status_date: date           # = Column(Date)
    # vip = Column(Boolean)
    # subject = Column(String(length=300))
    # deleted = Column(Boolean)
    # currency = Column(String(length=40))
    # policynumber = Column(String(length=40))
    # applicant = Column(String(length=255))
    # applicantemail = Column(String(length=255))
    # applicantphone = Column(String(length=255))
    # paiddate = Column(Date)
    # updateddate = Column(DateTime)
    # doc_number = Column(String(length=255))
    # doc_date = Column(Date)
    # claimdate = Column(DateTime)
    # currency_code = Column(String(length=10))
    # insurance_executor = Column(String(length=255))
    # broker_executor = Column(String(length=255))
    # insurance_delegate_reason = Column(String(length=255))
    # insured_delegate_reason = Column(String(length=255))
    # broker_delegate_reason = Column(String(length=255))
    # insured_executor = Column(String(length=255))
    # lru_date = Column(DateTime)
