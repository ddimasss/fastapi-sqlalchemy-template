from fastapi import Depends
from app.service.sqlalchemy import get_session_sync
from ..models.Okved import OkvedModel
from ..schemas.Okved import OkvedScheme


async def edit(
        okved_request: OkvedScheme,
        db=Depends(get_session_sync)
):
    obj = db.query(OkvedModel).get(okved_request.id)
    obj.name = okved_request.name
    obj.okved = okved_request.okved
    obj.description = okved_request.description
    obj.is_deleted = okved_request.is_deleted
    db.commit()

    return obj
