from fastapi import Depends
from app.service.sqlalchemy import get_session, get_session_sync
from sqlalchemy import select
from ..models.Okved import OkvedModel
from ..schemas.Okved import OkvedScheme


async def add(
        okved_request: OkvedScheme,
        db=Depends(get_session_sync)
):
    if 'id' in okved_request:
        del okved_request.id
    db_okved = OkvedModel(
        name=okved_request.name,
        okved=okved_request.okved,
        description=okved_request.description,
        is_deleted=0
    )
    db.add(db_okved)
    db.commit()
    db.refresh(db_okved)

    return db_okved
