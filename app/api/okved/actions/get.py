from fastapi import Depends
from app.service.sqlalchemy import get_session_sync
from ..models.Okved import OkvedModel


async def get(
        okved_id: int,
        db=Depends(get_session_sync)
):
    obj = db.query(OkvedModel).get(okved_id)
    db.commit()

    return obj


async def get_list(
        page: int = 0,
        limit: int = 0,
        db=Depends(get_session_sync)
):
    # generate({}, '')
    offset = page * limit
    obj = db.query(OkvedModel).limit(limit).offset(offset).all()
    res = obj

    return res
