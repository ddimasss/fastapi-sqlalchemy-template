from pydantic import BaseModel, Field
from typing import List, Optional, Any
from datetime import date


# CREATE TABLE public.positions (
# 	id serial4 NOT NULL,
# 	include_date date NOT NULL,
# 	exclude_date date NULL,
# 	description text NULL,
# 	name_i varchar NOT NULL,
# 	name_r varchar NOT NULL,
# 	name_d varchar NOT NULL,
# 	name_t varchar NOT NULL,
# 	name_v varchar NOT NULL,
# 	CONSTRAINT positions_pk PRIMARY KEY (id)
# );


class SdlScheme(BaseModel):
    class Config:
        from_attributes = True
    id: Optional[int] = None
    employee_id: int
    start_date: date
    end_date: Optional[date]
