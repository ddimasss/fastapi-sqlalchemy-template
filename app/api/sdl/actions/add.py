from fastapi import Depends
from app.service.sqlalchemy import get_session, get_session_sync
from ..models.Sdl import SdlModel
from ..schemas.Sdl import SdlScheme
import logging


async def add(
        sdl_request: SdlScheme,
        db=Depends(get_session_sync)
):
    if 'id' in sdl_request:
        del sdl_request.id
    db_employee = SdlModel(
        employee_id=sdl_request.employee_id,
        start_date=sdl_request.start_date,
        end_date=sdl_request.end_date
    )
    db.add(db_employee)
    db.commit()
    db.refresh(db_employee)

    return db_employee
