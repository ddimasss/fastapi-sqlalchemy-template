from fastapi import Depends
from app.service.sqlalchemy import get_session_sync
from ..models.Sdl import SdlModel
from ..schemas.Sdl import SdlScheme


async def edit(
        sdl_request: SdlScheme,
        db=Depends(get_session_sync)
):
    obj = db.query(SdlModel).get(sdl_request.id)
    obj.employee_id = sdl_request.employee_id
    obj.start_date = sdl_request.start_date
    obj.end_date = sdl_request.end_date
    db.commit()

    return obj
