from pydantic import (
    BaseSettings
)

ENV_PREFIX = 'DMA_SDL_'


class SdlSettings(BaseSettings):
    class Config:
        env_prefix = ENV_PREFIX


employee_settings = SdlSettings()