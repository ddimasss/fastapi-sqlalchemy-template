from sqlalchemy import Column, Integer, ForeignKey, Date, Sequence
from app.service.sqlalchemy import BaseAlch
from app.api.employee.models.Employee import EmployeeModel


class SdlModel(BaseAlch):
    __tablename__ = 'sdl'
    sdl_id_seq = Sequence('sdl_id_seq')
    id = Column(Integer, sdl_id_seq, server_default=sdl_id_seq.next_value(), primary_key=True)
    employee_id = Column(Integer, ForeignKey(EmployeeModel.id), nullable=False)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=True)
