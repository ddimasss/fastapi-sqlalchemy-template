from fastapi import APIRouter
# from .claims import claims_router
# from .reports_general import reports_general_router
from .get_status import get_status
#  from .generator import generator_router
from .company_type import company_type_router
from .okved import okved_router
from .message import message_router
from .customer import customer_router
from .tariff import tariff_router
from .employee import employee_router
from .instruction import instruction_router
from .position import position_router
from .sdl import sdl_router
from .settings import settings_router
from .template import template_router
from .customer_category import customer_category_router
from .send_logs import send_logs_router
from .users import user_router
from .generation import generation_router

api_router = APIRouter()
api_router.add_api_route(
    path='/status',
    endpoint=get_status,
    methods=['GET'],
    tags=['other']
)
#  api_router.include_router(generator_router, prefix='/generate')
api_router.include_router(generation_router, prefix='/gen')
api_router.include_router(company_type_router, prefix='/company_type')
api_router.include_router(okved_router, prefix='/okved')
api_router.include_router(message_router, prefix='/message')
api_router.include_router(customer_router, prefix='/customer')
api_router.include_router(tariff_router, prefix='/tariff')
api_router.include_router(employee_router, prefix='/employee')
api_router.include_router(instruction_router, prefix='/instruction')
api_router.include_router(position_router, prefix='/position')
api_router.include_router(sdl_router, prefix='/sdl')
api_router.include_router(settings_router, prefix='/settings')
api_router.include_router(template_router, prefix='/templates')
api_router.include_router(customer_category_router, prefix='/customer_category')
api_router.include_router(send_logs_router, prefix='/send_logs')
api_router.include_router(user_router, prefix='/users')
# api_router.include_router(reports_general_router, prefix='/reports')
