from pydantic import (
    BaseSettings
)

ENV_PREFIX = 'DMA_SENDLOGS_'


class SendLogsSettings(BaseSettings):
    class Config:
        env_prefix = ENV_PREFIX


employee_settings = SendLogsSettings()