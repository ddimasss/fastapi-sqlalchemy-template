from fastapi import Depends
from app.service.sqlalchemy import get_session_sync
from ..models.SendLogs import SendLogsModel
from sqlalchemy.orm import joinedload


async def get(
        log_id: int,
        db=Depends(get_session_sync)
):
    obj = db.query(SendLogsModel).get(log_id)
    db.commit()

    return obj


async def get_list(
        page: int = 0,
        limit: int = 0,
        db=Depends(get_session_sync)
):
    offset = page * limit
    obj = db.query(SendLogsModel)\
        .options(joinedload(SendLogsModel.user)) \
        .options(joinedload(SendLogsModel.customer)) \
        .options(joinedload(SendLogsModel.instruction)) \
        .order_by(SendLogsModel.id.desc()) \
        .limit(limit).offset(offset).all()
    res = obj

    return res
