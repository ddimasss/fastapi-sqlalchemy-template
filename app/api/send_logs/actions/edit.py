from fastapi import Depends
from app.service.sqlalchemy import get_session_sync
from ..models.SendLogs import SendLogsModel
from ..schemas.SendLogs import SendLogsScheme


async def edit(
        send_log_request: SendLogsScheme,
        db=Depends(get_session_sync)
):
    obj = db.query(SendLogsModel).get(send_log_request.id)
    obj.user_id = send_log_request.user_id
    obj.created_at = send_log_request.created_at
    obj.customer_id = send_log_request.customer_id
    obj.date = send_log_request.date
    obj.instruction_id = send_log_request.instruction_id
    obj.status = send_log_request.status
    db.commit()

    return obj
