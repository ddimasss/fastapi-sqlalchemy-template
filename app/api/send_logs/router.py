from fastapi import APIRouter
from .actions.add import add
from .actions.edit import edit
from .actions.get import get, get_list
from .schemas.SendLogs import SendLogsScheme
from typing import List


send_logs_router = APIRouter()


send_logs_router.add_api_route(
    path='/add',
    endpoint=add,
    methods=['POST'],
    tags=['other'],
    response_model=SendLogsScheme
)
send_logs_router.add_api_route(
    path='/edit',
    endpoint=edit,
    methods=['POST'],
    tags=['other'],
    response_model=SendLogsScheme
)

send_logs_router.add_api_route(
    path='/get',
    endpoint=get,
    methods=['GET'],
    tags=['other'],
    response_model=SendLogsScheme
)

send_logs_router.add_api_route(
    path='/get_list',
    endpoint=get_list,
    methods=['GET'],
    tags=['other'],
    response_model=List[SendLogsScheme]
)
