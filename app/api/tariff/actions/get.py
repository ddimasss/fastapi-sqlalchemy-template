from fastapi import Depends
from app.service.sqlalchemy import get_session_sync
from ..models.Tariff import TariffModel


async def get(
        tariff_id: int,
        db=Depends(get_session_sync)
):
    obj = db.query(TariffModel).get(tariff_id)
    db.commit()

    return obj


async def get_list(
        page: int = 0,
        limit: int = 0,
        db=Depends(get_session_sync)
):
    offset = page * limit
    obj = db.query(TariffModel).limit(limit).offset(offset).all()
    res = obj

    return res
