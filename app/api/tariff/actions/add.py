from fastapi import Depends
from app.service.sqlalchemy import get_session, get_session_sync
from sqlalchemy import select
from ..models.Tariff import TariffModel
from ..schemas.Tariff import TariffScheme


async def add(
        okved_request: TariffScheme,
        db=Depends(get_session_sync)
):
    if 'id' in okved_request:
        del okved_request.id
    db_okved = TariffModel(
        name=okved_request.name,
        description=okved_request.description,
        is_deleted=0
    )
    db.add(db_okved)
    db.commit()
    db.refresh(db_okved)

    return db_okved
