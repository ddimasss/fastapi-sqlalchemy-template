from pydantic import BaseModel, Field
from enum import Enum
from datetime import date
from typing import List, Optional, Any

# CREATE TABLE public.tariffs (
# 	id serial NOT NULL,
# 	"name" varchar NOT NULL,
# 	description text NULL,
# 	is_deleted bool NOT NULL DEFAULT false,
# 	CONSTRAINT tariffs_pk PRIMARY KEY (id)
# );


class TariffScheme(BaseModel):
    class Config:
        from_attributes = True
    id: Optional[int] = None
    name: str
    description: Optional[str] = None
    is_deleted: Optional[bool] = False
