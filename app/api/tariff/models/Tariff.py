from sqlalchemy import Column, Integer, String, Boolean
from app.service.sqlalchemy import BaseAlch


# CREATE TABLE public.tariffs (
# 	id serial NOT NULL,
# 	"name" varchar NOT NULL,
# 	description text NULL,
# 	is_deleted bool NOT NULL DEFAULT false,
# 	CONSTRAINT tariffs_pk PRIMARY KEY (id)
# );

class TariffModel(BaseAlch):
    __tablename__ = 'tariffs'
    id = Column(Integer, primary_key=True)
    name = Column(String())
    description = Column(String())
    is_deleted = Column(Boolean())
