from fastapi import APIRouter
from .actions.get_documnets import get_docs


generation_router = APIRouter()


generation_router.add_api_route(
    path='/get_documents',
    endpoint=get_docs,
    methods=['POST'],
    tags=['other']
)
