from fastapi import Depends
from app.service.sqlalchemy import get_session, get_session_sync
from sqlalchemy import select
from ..models.Instructions import InstructionModel
from ..schemas.Instructions import InstructionScheme


async def add(
        instruction_request: InstructionScheme,
        db=Depends(get_session_sync)
):
    if 'id' in instruction_request:
        del instruction_request.id
    db_instruction = InstructionModel(
        name=instruction_request.name,
        short_name=instruction_request.short_name,
        description=instruction_request.description,
        is_deleted=0
    )
    db.add(db_instruction)
    db.commit()
    db.refresh(db_instruction)

    return db_instruction
