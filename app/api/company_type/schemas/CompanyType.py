from pydantic import BaseModel, Field
from enum import Enum
from datetime import date
from typing import List, Optional, Any


class Role(str, Enum):
    '''Organization role'''
    transportCompany = 'transportCompany'
    insuranceCompany = 'insuranceCompany'
    broker = 'broker'


class GroupBy(str, Enum):
    ''' Группировка по полям
        - 'Тип страхования'
        - 'Предмет обращения'
        - 'Статус претензии'
    '''
    insurance_type = 'insurance_type'  # 'Тип страхования'
    subject = 'subject'  # 'Предмет обращения'
    status = 'status'  # 'Статус претензии'


class Operations(str, Enum):
    in_ = 'in_'
    like = 'like'
    eq = '__eq__'


class Filter(BaseModel):
    name: str
    operation: Operations
    values: Any

    class Config:
        use_enum_values = True


class ClaimRequest(BaseModel):
    role: Role = Field(...)
    aggregate_by: Optional[List[GroupBy]] = []
    filters: Optional[List[Filter]] = []

    # "mspName": "DellinMSP"
    # "mspNames": ["TheDummyMSP"]
    # "dateRange": []
    #  "insuranceTypes": ["Грузы", "Неизвестно"]
    # "claimStatuses": []
    # "claimSubjects": []
    # "transportCompanies": []
    # "incuranceCompanies": []
    # "currencyCode": "RUB"
    # "aggregateTypes": []
    # "vipStatus": false
    class Config:
        use_enum_values = True


class CompanyType(BaseModel):
    class Config:
        from_attributes = True
    id: Optional[int] = None
    full_name: str
    short_name: str
    description: Optional[str] = None
    is_deleted: Optional[bool] = False
