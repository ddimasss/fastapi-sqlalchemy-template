from typing import Optional
from app.service.sqlalchemy import get_session_sync
from fastapi import Depends, Form, Request, UploadFile
from ..models.Template import TemplateModel, TemplateToCompanyType, TemplateToCustomerCategory
from ..schemas.Template import TemplateAddScheme
from app.settings import settings
import cProfile
import re
import os


async def add_upload_file(
        db=Depends(get_session_sync),
        local_data=Form(...),
        file: Optional[bytes] = None
    ):
    with cProfile.Profile() as pr:
        template = TemplateAddScheme.parse_raw(local_data)
        clean_name = clean_string(template.name)
        clean_file_name = clean_string(template.file_path)
        file_name = settings.TEMPLATE_FILES_PATH + clean_name + "_" + clean_file_name
        if file:
            if os.path.exists(file_name):
                raise ValueError("Same template name")
            else:
                with open(file_name, 'wb+') as f:
                    f.write(await file.read())
            template.file_path = file_name
        sql_template_model = TemplateModel()
        sql_template_model.from_pydantic(template)
        template_to_company_type = TemplateToCompanyType()
        template_to_company_type.company_type_id = template.company_type_id
        db.merge(sql_template_model)
        db.flush()
        db.query(TemplateToCustomerCategory).filter(TemplateToCustomerCategory.template_id == template.id).delete()
        db.query(TemplateToCompanyType).filter(TemplateToCompanyType.template_id == template.id).delete()
        db.flush()
        for cust_cat in template.customer_categories:
            templ_2_cust_cat = TemplateToCustomerCategory(
                customer_category_id=cust_cat,
                template_id=sql_template_model.id
            )
            db.add(templ_2_cust_cat)
        template_to_company_type.template_id = sql_template_model.id
        db.add(template_to_company_type)
        db.commit()
        db.refresh(sql_template_model)
    pr.print_stats()
    return sql_template_model


async def edit_upload_file(
        request: Request,
        db=Depends(get_session_sync),
    ):
    body = await request.form()
    template = TemplateAddScheme.parse_raw(body['local_data'])
    if template.action == 'Update':
        sql_template_model = db.query(TemplateModel).get(template.id)
    if template.action == 'Add':
        sql_template_model = TemplateModel()
    if 'file' in body:
        if sql_template_model.file_path is not None and os.path.exists(sql_template_model.file_path):
            os.remove(sql_template_model.file_path)
        clean_name = clean_string(template.name)
        clean_file_name = clean_string(template.file_path)
        file_name = settings.TEMPLATE_FILES_PATH + clean_name + "_" + clean_file_name
        if os.path.exists(file_name):
            raise ValueError("Same template name")
        else:
            with open(file_name, 'wb+') as f:
                f.write(body['file'].file.read())
        template.file_path = file_name
    sql_template_model.from_pydantic(template)
    template_to_company_type = TemplateToCompanyType()
    template_to_company_type.company_type_id = template.company_type_id
    if template.action == 'Update':
        db.merge(sql_template_model)
    else:
        db.add(sql_template_model)
    db.flush()
    db.query(TemplateToCustomerCategory).filter(TemplateToCustomerCategory.template_id == sql_template_model.id).delete()
    db.flush()
    for cust_cat in template.customer_categories:
        templ_2_cust_cat = TemplateToCustomerCategory(
            customer_category_id=cust_cat,
            template_id=sql_template_model.id
        )
        db.add(templ_2_cust_cat)
    db.query(TemplateToCompanyType).filter(TemplateToCompanyType.template_id == sql_template_model.id).delete()
    db.flush()
    template_to_company_type.template_id = sql_template_model.id
    db.add(template_to_company_type)
    db.commit()
    db.refresh(sql_template_model)
    return template


def clean_string(s):
    res = s.strip()
    res = res.replace(' ', '_')
    res = re.sub('[^A-Za-zА-Яа-я0-9_\.]', '', res)
    res = res.strip('_')
    return res

