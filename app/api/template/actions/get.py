from fastapi import Depends
from app.service.sqlalchemy import get_session_sync
from sqlalchemy.sql import text


async def get(
        template_id: int,
        db=Depends(get_session_sync)
):
    obj = db.execute(text('''
        select 
            t.*, 
            ttct.company_type_id, 
            array_agg(ttcc.customer_category_id) as customer_categories
        from templates t
        left join template_to_company_type ttct on (t.id = ttct.template_id)
        left join template_to_customer_category ttcc on (t.id = ttcc.template_id)
        where t.id = :template_id
        group by t.id, ttct.company_type_id
        '''), {'template_id': template_id}).first()
    #obj.company_type_id = obj.company_type[0].company_type_id
    print('1', obj)
    db.commit()

    return obj


async def get_list(
        page: int = 0,
        limit: int = 0,
        db=Depends(get_session_sync)
):
    offset = page * limit
    objs = db.execute(text('''
            select 
                t.*, 
                ttct.company_type_id, 
                array_agg(ttcc.customer_category_id) as customer_categories
            from templates t
            left join template_to_company_type ttct on (t.id = ttct.template_id)
            left join template_to_customer_category ttcc on (t.id = ttcc.template_id)
            group by t.id, ttct.company_type_id
            order by t.id
            limit :limit
            offset :offset
            '''), {'limit': limit, 'offset': offset}).all()

    return objs
