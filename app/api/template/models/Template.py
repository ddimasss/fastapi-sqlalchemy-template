from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, Date, Sequence
from sqlalchemy.orm import relationship
from app.service.sqlalchemy import BaseAlch
from app.api.instruction.models.Instructions import InstructionModel
from app.api.tariff.models.Tariff import TariffModel
from app.api.message.models.Message import MessageModel
from app.api.customer_category.models.CustomerCategory import CustomerCategoryModel
from app.api.company_type.models.CompanyType import CompanyTypeSqlA
from ..schemas.Template import TemplateAddScheme


class TemplateToTariffs(BaseAlch):
    __tablename__ = 'template_to_tariff'
    template_id = Column(Integer, ForeignKey("templates.id"), nullable=False, primary_key=True)
    tariff_id = Column(Integer, ForeignKey(TariffModel.id), nullable=False, primary_key=True)


class TemplateToMessage(BaseAlch):
    __tablename__ = 'template_to_message'
    template_id = Column(Integer, ForeignKey("templates.id"), nullable=False, primary_key=True)
    message_id = Column(Integer, ForeignKey(MessageModel.id), nullable=False, primary_key=True)


class TemplateToCustomerCategory(BaseAlch):
    __tablename__ = 'template_to_customer_category'
    template_id = Column(Integer, ForeignKey("public.templates.id"), nullable=False, primary_key=True)
    customer_category_id = Column(Integer, ForeignKey(CustomerCategoryModel.id), nullable=False, primary_key=True)


class TemplateToCompanyType(BaseAlch):
    __tablename__ = 'template_to_company_type'
    template_id = Column(Integer, ForeignKey("public.templates.id"), nullable=False, primary_key=True)
    company_type_id = Column(Integer, ForeignKey(CompanyTypeSqlA.id), nullable=False, primary_key=True)


class TemplateModel(BaseAlch):
    __tablename__ = 'templates'
    customers_id_seq = Sequence('customers_id_seq')
    id = Column(Integer, customers_id_seq, server_default=customers_id_seq.next_value(), primary_key=True)
    name = Column(String(), nullable=False)
    employee_count = Column(Integer, nullable=False)
    file_path = Column(String(), nullable=False)
    message = Column(String(), nullable=False)
    doc_start_date = Column(Date, nullable=False)
    gen_start_date = Column(Date, nullable=True)
    npa_date = Column(Date, nullable=True)
    is_deleted = Column(Boolean, nullable=False)
    instruction_id = Column(Integer, ForeignKey(InstructionModel.id), nullable=False)
    npa_name = Column(String(), nullable=False)
    company_type = relationship("TemplateToCompanyType")

    def from_pydantic(self, scheme: TemplateAddScheme):
        public_names = [
            "id",
            "name",
            "employee_count",
            "file_path",
            "message",
            "doc_start_date",
            "gen_start_date",
            "npa_date",
            "is_deleted",
            "instruction_id",
            "npa_name"
        ]
        for name in public_names:
            attr = getattr(scheme, name, None)
            if attr is not None:
                setattr(self, name, attr)
