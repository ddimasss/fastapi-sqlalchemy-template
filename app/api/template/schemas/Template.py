from fastapi import UploadFile
from pydantic import BaseModel, Field
from enum import Enum
from typing import List, Optional, Any
from datetime import date


class TemplateScheme(BaseModel):
    class Config:
        from_attributes = True
    id: Optional[int] = None
    name: str
    employee_count: int
    file_path: str
    message: str
    doc_start_date: date
    gen_start_date: Optional[date]
    npa_date: Optional[date]
    is_deleted: Optional[bool] = False
    instruction_id: int
    npa_name: str
    company_type_id: Optional[int]
    customer_categories: Optional[List[int]]



class TemplateAddScheme(BaseModel):
    class Config:
        from_attributes = True
    id: Optional[int] = None
    name: str
    employee_count: int
    message: str
    doc_start_date: date
    gen_start_date: Optional[date]
    npa_date: Optional[date]
    is_deleted: Optional[bool] = False
    instruction_id: int
    npa_name: str
    company_type_id: int
    customer_categories: List[int]
    file_path: str
    action: Optional[str]
