from pydantic import (
    BaseSettings
)

ENV_PREFIX = 'DMA_TEMPLATE_'


class TemplateSettings(BaseSettings):
    class Config:
        env_prefix = ENV_PREFIX


claim_settings = TemplateSettings()