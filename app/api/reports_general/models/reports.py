from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from ..db import Base
from .report_groups import Module
from .report_types import ReportType


class Report(Base):
    __tablename__ = 'reports' # noqa
    id = Column(Integer, primary_key=True)
    name = Column(String(length=255))
    description = Column(String)
    name_eng = Column(String(length=255))
    fa_icon_name = Column(String(length=40))
    module_id = Column(Integer, ForeignKey(Module.id), nullable=False)
    module = relationship('Module', back_populates='reports')
    type_id = Column(Integer, ForeignKey(ReportType.id), nullable=False)
    type = relationship('ReportType', back_populates='reports')
    filters = relationship('Filter')
    favorites = relationship('Favorite')
    #tag = Column(String(length=255))
