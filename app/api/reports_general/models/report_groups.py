from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship
from ..db import Base


class Module(Base):
    __tablename__ = 'report_modules' # noqa
    id = Column(Integer, primary_key=True)
    name = Column(String(length=255))
    description = Column(String)
    name_eng = Column(String(length=255))
    fa_icon_name = Column(String(length=40))
    reports = relationship("Report")
