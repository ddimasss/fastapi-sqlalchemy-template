from fastapi import Depends

from app.service.sqlalchemy import get_session_sync
from ..models.Customer import CustomerModel
from app.api.position.models.Position import PositionModel
import psycopg2

async def get(
        customer_id: int,
        db=Depends(get_session_sync)
):
    obj = db.query(CustomerModel).get(customer_id)
    db.commit()

    return obj


async def get_list(
        page: int = 0,
        limit: int = 0,
        db=Depends(get_session_sync)
):
    offset = page * limit
    obj = db.query(CustomerModel).filter(CustomerModel.is_deleted.is_not(True)).order_by(CustomerModel.id).limit(
        limit).offset(offset).all()
    res = [row.to_dict() for row in obj]

    return res


async def get_all_by_customer_id(
        customer_id: int,
        db=Depends(get_session_sync)
):
    sql = '''
select
       c.*,
       cto.oks as okveds,
       p.poss as positions,
       e.empls as employees,
       a.addrs as addresses,
       ctcc.customer_category_ids
from customers c
left join (
            select
                customer_id,
                array_agg(
                    json_build_object(
                        'id', id,
                        'address', address,
                        'date_start', date_start,
                        'date_end', date_end,
                        'is_deleted', "is_deleted",
                        'type', "type",
                        'customer_id', customer_id
                        )
                    order by "type"
                    ) as addrs
            from addresses
            where customer_id = %(customer_id)s
            group by customer_id
            ) a on c.id = a.customer_id
left join (
    select customer_id,
           array_agg(
               json_build_object(
                   'okved_id', okved_id,
                   'customer_id', customer_id,
                   'type', "type",
                   'date_start', "date_start",
                   'date_end', date_end
                   )
               ) as oks
    from customer_to_okveds
    where customer_id = %(customer_id)s
    group by customer_id
    ) cto on c.id = cto.customer_id
left join (
    select customer_id,
           json_agg(json_build_object(
               'customer_id', p1.customer_id,
               'id', p1.id,
               'sdl', p1.sdl,
               'description', p1.description,
               'end_date', p1.end_date,
               'name_d', p1.name_d,
               'name_i', p1.name_i,
               'name_v', p1.name_v,
               'name_t', p1.name_t,
               'name_r', p1.name_r,
               'start_date', p1.start_date,
               'seo', p1.seo
               )
               ) as poss
    from positions p1
    where customer_id = %(customer_id)s
    group by customer_id
    ) p on c.id = p.customer_id
left join (
    select customer_id,
           json_agg(json_build_object(
               'id', e1.id,
               'end_date', e1.end_date,
               'customer_id', e1.customer_id,
               'ci_end_date', e1.ci_end_date,
               'f_name', e1.f_name,
               'l_name', e1.l_name,
               'l_name_d', e1.l_name_d,
               'l_name_r', e1.l_name_r,
               'l_name_t', e1.l_name_t,
               'l_name_v', e1.l_name_v,
               's_name', e1.s_name,
               'sdl_start_date', e1.sdl_start_date,
               'start_date', e1.start_date,
               'l_name_i', e1.l_name,
               'position_id', etp.position_id,
               'position', etp.position_id
               )) as empls
    from employees e1
    left join employees_to_positions etp on e1.id = etp.employee_id
    where customer_id = %(customer_id)s
    group by customer_id
    ) e on c.id = e.customer_id
left join (
    select customer_id,
           array_agg(customer_to_customer_category.customer_category_id) as customer_category_ids
    from customer_to_customer_category
    where customer_id = %(customer_id)s
    group by customer_id
    ) ctcc on c.id = ctcc.customer_id
where c.id=%(customer_id)s
    '''
    conn = psycopg2.connect(database="finconsultcrm", user="finconsultcrm",
                            password="Windowsmustdie1~", host="188.168.35.10", port=55432)
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cur.execute(sql, {'customer_id': customer_id})
    records = cur.fetchall()

    main = {
        'id': records[0]['id'],
        'company_type_id': records[0]['company_type_id'],
        'kpp': records[0]['company_type_id'],
        'itn': records[0]['itn'],
        'ogrn': records[0]['ogrn'],
        'timezone': records[0]['time_zone'],
        'customer_categories': records[0]['customer_category_ids'],
        'full_name': records[0]['full_name'],
        'email': records[0]['email'],
        'short_name': records[0]['short_name'],
        'phone': records[0]['phone'],
        'addresses': records[0]['addresses'],
    }
    main2 = {
        "contract_number": records[0]['contract_number'],
        "contract_date_start": records[0]['contract_date_start'],
        "contract_date_end": records[0]['contract_date_end'],
        "tariff_id": records[0]['tariff_id'],
        "contact_name": records[0]['contact_name'],
        "contact_phone": records[0]['contact_phone'],
        "contact_email": records[0]['contact_email'],
        "description": records[0]['description'],
    }
    main_okveds = []
    other_okveds = []
    for okved in records[0]['okveds']:
        if okved['type'] == 'main':
            main_okveds.append(okved)
        if okved['type'] == 'other':
            other_okveds.append(okved)
    positions = records[0]['positions']
    employees = records[0]['employees']
    print(positions)
    return {'main': main,
            'main2': main2,
            'mainOkveds': main_okveds,
            'otherOkveds': other_okveds,
            'staffstruct': positions,
            'staff': employees
            }


async def get_staff_struct_by_id(
        customer_id: int,
        page: int = 0,
        limit: int = 0,
        db=Depends(get_session_sync)
):
    offset = page * limit
    obj = db.query(PositionModel) \
        .where(PositionModel.customer_id == customer_id) \
        .limit(limit).offset(offset).all()
    db.commit()
    res = obj

    return res
