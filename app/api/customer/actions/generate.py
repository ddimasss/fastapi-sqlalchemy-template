from ..schemas.Customer import CustomerSchemeGen
from ..models.Customer import CustomersToCustomerCategory
from app.service.rabbitmq import producer
from app.service.sqlalchemy import get_session_sync
from fastapi import Depends


async def gen(
        generate_request: CustomerSchemeGen,
):
    await producer(
        customer_id=generate_request.id,
        gen_date=generate_request.date,
        ids=generate_request.instruction_ids,
        user_id=generate_request.user_id
    )
    return {'result': 'started'}


async def gen_many(
        generate_request: CustomerSchemeGen,
        db=Depends(get_session_sync)
):
    cat_id = generate_request.id
    cust2cats = db.query(CustomersToCustomerCategory) \
        .filter(CustomersToCustomerCategory.customer_category_id == cat_id).all()
    for cust2cut in cust2cats:
        await producer(
            customer_id=cust2cut.customer_id,
            gen_date=generate_request.date,
            ids=generate_request.instruction_ids,
            user_id=generate_request.user_id
        )
    return {'result': 'started'}
