from fastapi import Depends, Request
from psycopg2 import IntegrityError

from app.service.sqlalchemy import get_session, get_session_sync
from sqlalchemy import select
from ..models.Customer import CustomerModel, Address, CustomersToOkved, CustomersToCustomerCategory
from app.api.position.models.Position import PositionModel
from app.api.employee.models.Employee import EmployeeModel
from app.api.employee.models.EmployeeToPosition import EmployeeToPositionsModel
from ..schemas.Customer import CustomerSchemeIn
import orjson
from app.generator.main import generate_for_new_customer


async def create_customer(
        body: Request,
        db=Depends(get_session_sync)
):
    req = orjson.loads(await body.body())
    try:
        db_customer = CustomerModel(
            itn=req['main']['itn'],
            company_type_id=req['main']['company_type_id'],
            full_name=req['main']['full_name'],
            time_zone=req['main']['timezone'],
            contract_date_start=req['main2']['contract_date_start'],
            contract_date_end=req['main2']['contract_date_end'],
            contract_number=req['main2']['contract_number'],
            short_name=req['main']['short_name'],
            ogrn=req['main']['ogrn'],
            kpp=req['main']['kpp'],
            phone=req['main']['phone'],
            email=req['main']['email'],
            is_deleted=False,
            contact_name=req['main2']['contact_name'] if 'contact_name' in req['main2'] else None,
            contact_phone=req['main2']['contact_phone'] if 'contact_phone' in req['main2'] else None,
            contact_email=req['main2']['contact_email'] if 'contact_email' in req['main2'] else None,
            description=req['main2']['description'] if 'description' in req['main2'] else None,
            ros_fin_login=req['main2']['rosFinLogin'] if 'rosFinLogin' in req['main2'] else None,
            ros_fin_pass=req['main2']['rosFinPass'] if 'rosFinPass' in req['main2'] else None,
            tariff_id=req['main2']['tariff_id']
        )
        db.add(db_customer)
        db.flush()
        for addr in req['main']['addresses']:
            address = Address(
                address=addr['address'],
                date_start=addr['date_start'],
                date_end=addr['date_end'] if ('date_end' in addr and addr['date_end'] != '') else None,
                is_deleted=False,
                type=addr['type'],
                customer_id=db_customer.id
            )
            db.add(address)
        for category_id in req['main']['customer_categories']:
            print(category_id)
            customer_to_customer_category = CustomersToCustomerCategory(
                customer_id=db_customer.id,
                customer_category_id=category_id
            )
            db.add(customer_to_customer_category)

        for okved in req['mainOkveds']:
            link_to_okved = CustomersToOkved(
                customer_id=db_customer.id,
                okved_id=okved['okved_id'],
                type='main',
                date_start=okved['date_start'],
                date_end=okved['date_end'] if 'date_end' in okved and okved['date_end'] != '' else None
            )
            db.add(link_to_okved)
        for okved in req['otherOkveds']:
            link_to_okved = CustomersToOkved(
                customer_id=db_customer.id,
                okved_id=okved['okved_id'],
                type='other',
                date_start=okved['date_start'],
                date_end=okved['date_end'] if 'date_end' in okved and okved['date_end'] != '' else None
            )
            db.add(link_to_okved)
        links = {}
        for staffstruct in req['staffstruct']:
            position = PositionModel(
                name_r=staffstruct['name_r'],
                name_i=staffstruct['name_i'],
                name_d=staffstruct['name_d'],
                name_v=staffstruct['name_v'],
                name_t=staffstruct['name_t'],
                description=staffstruct['description'] if 'description' in staffstruct else None,
                start_date=staffstruct['start_date'],
                end_date=staffstruct['end_date'] if (
                            ('end_date' in staffstruct) and (staffstruct['end_date'] != '')) else None,
                customer_id=db_customer.id,
                seo=staffstruct['seo'] if 'seo' in staffstruct and staffstruct['seo'] != '' else False,
                sdl=staffstruct['sdl'] if 'sdl' in staffstruct and staffstruct['sdl'] != '' else False,
            )
            db.add(position)
            db.flush()
            links[staffstruct['id']] = position.id
        for staff in req['staff']:
            employee = EmployeeModel(
                f_name=staff['f_name'],
                l_name=staff['l_name_i'],
                s_name=staff['s_name'],
                l_name_r=staff['l_name_r'],
                l_name_d=staff['l_name_d'],
                l_name_v=staff['l_name_v'],
                l_name_t=staff['l_name_t'],
                customer_id=db_customer.id,
                start_date=staff['start_date'],
                end_date=staff['end_date'] if 'end_date' in staff and staff['end_date'] != '' else None,
                ci_end_date=staff['ci_end_date'] if 'ci_end_date' in staff and staff['ci_end_date'] != '' else None,
                sdl_start_date=staff['sdl_start_date'] if 'sdl_start_date' in staff and staff[
                    'sdl_start_date'] != '' else None
            )
            db.add(employee)
            db.flush()
            employee_to_position = EmployeeToPositionsModel(
                employee_id=employee.id,
                position_id=links[staff['position_id']],
                start_date=staff['start_date'],
                end_date=staff['end_date'] if 'end_date' in staff and staff['end_date'] != '' else None
            )
            db.add(employee_to_position)
        db.commit()
    except IntegrityError as err:
        db.rollback()
        return {'result': 'error', 'msg': err}

    return {'result': 'ok'}
