from pydantic import (
    BaseSettings
)

ENV_PREFIX = 'DMA_CUSTOMER_'


class ClaimSettings(BaseSettings):
    class Config:
        env_prefix = ENV_PREFIX


claim_settings = ClaimSettings()