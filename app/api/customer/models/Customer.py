from sqlalchemy import Column, Integer, String, Boolean, ForeignKey, Date, Sequence
from app.service.sqlalchemy import BaseAlch
from app.api.company_type.models.CompanyType import CompanyTypeSqlA
from app.api.tariff.models.Tariff import TariffModel
from app.api.customer_category.models.CustomerCategory import CustomerCategoryModel
from sqlalchemy_serializer import SerializerMixin


class CustomerModel(BaseAlch, SerializerMixin):
    __tablename__ = 'customers'
    customers_id_seq = Sequence('customers_id_seq')
    id = Column(Integer, customers_id_seq, server_default=customers_id_seq.next_value(), primary_key=True)
    itn = Column(String(), nullable=False)
    company_type_id = Column(Integer, ForeignKey(CompanyTypeSqlA.id), nullable=False)
    full_name = Column(String, nullable=False)
    time_zone = Column(Integer, nullable=False)
    contract_date_start = Column(Date, nullable=False)
    contract_date_end = Column(Date, nullable=False)
    contract_number = Column(String, nullable=False)
    short_name = Column(String, nullable=False)
    email = Column(String, nullable=True)
    ogrn = Column(String)
    kpp = Column(String)
    phone = Column(String)
    is_deleted = Column(Boolean, nullable=False)
    tariff_id = Column(Integer, ForeignKey(TariffModel.id), nullable=False)
    contact_name = Column(String, nullable=True)
    contact_phone = Column(String, nullable=True)
    contact_email = Column(String, nullable=True)
    description = Column(String, nullable=True)
    ros_fin_login = Column(String, nullable=True)
    ros_fin_pass = Column(String, nullable=True)


class CustomersToCustomerCategory(BaseAlch):
    __tablename__ = 'customer_to_customer_category'
    customer_id = Column(Integer, ForeignKey(CustomerModel.id), nullable=False, primary_key=True)
    customer_category_id = Column(Integer, ForeignKey(CustomerCategoryModel.id), nullable=False, primary_key=True)


class CustomersToOkved(BaseAlch):
    __tablename__ = 'customer_to_okveds'
    customer_id = Column(Integer, ForeignKey(CustomerModel.id), nullable=False, primary_key=True)
    okved_id = Column(Integer, ForeignKey('public.okveds.id'), nullable=False, primary_key=True)
    type = Column(String, nullable=False, default='other')
    date_start = Column(Date, nullable=False)
    date_end = Column(Date)


class Address(BaseAlch):
    __tablename__ = 'addresses'
    addresses_id_seq = Sequence('addresses_id_seq')
    id = Column(Integer, addresses_id_seq, server_default=addresses_id_seq.next_value(), primary_key=True)
    address = Column(String, nullable=False, default='')
    date_start = Column(Date, nullable=False)
    date_end = Column(Date, nullable=True)
    is_deleted = Column(Boolean, nullable=False, default=False, server_default="false")
    type = Column(String, nullable=False)
    customer_id = Column(Integer, ForeignKey('public.customers.id'), nullable=False)
