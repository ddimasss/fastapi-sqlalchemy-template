from pydantic import (
    BaseSettings
)

ENV_PREFIX = 'DMA_OKVED_'


class ClaimSettings(BaseSettings):
    class Config:
        env_prefix = ENV_PREFIX


claim_settings = ClaimSettings()