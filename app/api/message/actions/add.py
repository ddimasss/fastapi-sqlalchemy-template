from fastapi import Depends
from app.service.sqlalchemy import get_session, get_session_sync
from sqlalchemy import select
from ..models.Message import MessageModel
from ..schemas.Message import MessageScheme


async def add(
        message_request: MessageScheme,
        db=Depends(get_session_sync)
):
    if 'id' in message_request:
        del message_request.id
    db_message = MessageModel(
        name=message_request.name,
        text=message_request.text,
        is_deleted=0
    )
    db.add(db_message)
    db.commit()
    db.refresh(db_message)

    return db_message
