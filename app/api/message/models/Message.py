from sqlalchemy import Column, Integer, String, Boolean
from app.service.sqlalchemy import BaseAlch


class MessageModel(BaseAlch):
    __tablename__ = 'messages'
    id = Column(Integer, primary_key=True)
    name = Column(String())
    text = Column(String())
    is_deleted = Column(Boolean())
