from sqlalchemy import Column, Integer, String, ForeignKey, Date, Sequence
from app.service.sqlalchemy import BaseAlch
from app.api.customer.models.Customer import CustomerModel


class EmployeeModel(BaseAlch):
    __tablename__ = 'employees'
    employees_id_seq = Sequence('employees_id_seq')
    id = Column(Integer, employees_id_seq, server_default=employees_id_seq.next_value(), primary_key=True)
    f_name = Column(String, nullable=False)
    l_name = Column(String, nullable=False)
    s_name = Column(String, nullable=True)
    l_name_r = Column(String, nullable=False)
    l_name_d = Column(String, nullable=False)
    l_name_v = Column(String, nullable=False)
    l_name_t = Column(String, nullable=False)
    customer_id = Column(Integer, ForeignKey(CustomerModel.id), nullable=False)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=True)
    ci_end_date = Column(Date, nullable=True)
    sdl_start_date = Column(Date, nullable=True)
