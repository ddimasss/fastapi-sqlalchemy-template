from pydantic import (
    BaseSettings
)

ENV_PREFIX = 'DMA_EMPLOYEE_'


class EmployeeSettings(BaseSettings):
    class Config:
        env_prefix = ENV_PREFIX


employee_settings = EmployeeSettings()