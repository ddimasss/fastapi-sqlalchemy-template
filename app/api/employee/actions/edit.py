from fastapi import Depends
from app.service.sqlalchemy import get_session_sync
from ..models.Employee import EmployeeModel
from ..schemas.Employee import EmployeeScheme


async def edit(
        employee_request: EmployeeScheme,
        db=Depends(get_session_sync)
):
    obj = db.query(EmployeeModel).get(employee_request.id)
    obj.f_name = employee_request.f_name
    obj.l_name = employee_request.l_name
    obj.s_name = employee_request.s_name
    obj.l_name_r = employee_request.l_name_r
    obj.l_name_d = employee_request.l_name_d
    obj.l_name_v = employee_request.l_name_v
    obj.l_name_t = employee_request.l_name_t
    obj.start_date = employee_request.start_date
    obj.end_date = employee_request.end_date
    db.commit()

    return obj
