from pydantic import BaseModel, Field
from typing import List, Optional, Any
from datetime import date


# CREATE TABLE public.positions (
# 	id serial4 NOT NULL,
# 	include_date date NOT NULL,
# 	exclude_date date NULL,
# 	description text NULL,
# 	name_i varchar NOT NULL,
# 	name_r varchar NOT NULL,
# 	name_d varchar NOT NULL,
# 	name_t varchar NOT NULL,
# 	name_v varchar NOT NULL,
# 	CONSTRAINT positions_pk PRIMARY KEY (id)
# );


class PositionScheme(BaseModel):
    class Config:
        from_attributes = True
    id: Optional[int] = None
    name_i: str
    name_r: str
    name_d: str
    name_v: str
    name_t: str
    description: Optional[str]
    start_date: date
    end_date: Optional[date]
    customer_id: int
    seo: bool
    sdl: bool
