from fastapi import Depends
from app.service.sqlalchemy import get_session_sync
from ..models.Position import PositionModel


async def get(
        position_id: int,
        db=Depends(get_session_sync)
):
    obj = db.query(PositionModel).get(position_id)
    db.commit()

    return obj


async def get_list(
        page: int = 0,
        limit: int = 0,
        unique: int = 0,
        db=Depends(get_session_sync)
):
    offset = page * limit
    if unique == 1:
        obj = db.query(PositionModel).distinct(PositionModel.name_i).order_by(PositionModel.name_i).limit(limit).offset(offset).all()
    else:
        obj = db.query(PositionModel).order_by(PositionModel.name_i).limit(limit).offset(offset).all()
    res = obj

    return res
