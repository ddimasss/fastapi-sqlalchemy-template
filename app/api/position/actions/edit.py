from fastapi import Depends
from app.service.sqlalchemy import get_session_sync
from ..models.Position import PositionModel
from ..schemas.Position import PositionScheme


async def edit(
        position_request: PositionScheme,
        db=Depends(get_session_sync)
):
    obj = db.query(PositionModel).get(position_request.id)
    obj.name_i = position_request.name_i
    obj.name_r = position_request.name_r
    obj.name_d = position_request.name_d
    obj.name_v = position_request.name_v
    obj.name_t = position_request.name_t
    obj.description = position_request.description
    obj.start_date = position_request.start_date
    obj.end_date = position_request.end_date
    db.commit()

    return obj
