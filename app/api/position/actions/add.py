from fastapi import Depends
from app.service.sqlalchemy import get_session, get_session_sync
from ..models.Position import PositionModel
from ..schemas.Position import PositionScheme
import logging


async def add(
        position_request: PositionScheme,
        db=Depends(get_session_sync)
):
    logging.error(position_request)
    if 'id' in position_request:
        del position_request.id
    db_employee = PositionModel(
        name_i=position_request.name_i,
        name_r=position_request.name_r,
        name_d=position_request.name_d,
        name_v=position_request.name_v,
        name_t=position_request.name_t,
        description=position_request.description,
        start_date=position_request.start_date,
        end_date=position_request.end_date
    )
    db.add(db_employee)
    db.commit()
    db.refresh(db_employee)

    return db_employee
