from pydantic import (
    BaseSettings
)

ENV_PREFIX = 'DMA_POSITION_'


class PositionSettings(BaseSettings):
    class Config:
        env_prefix = ENV_PREFIX


employee_settings = PositionSettings()