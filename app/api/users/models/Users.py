from sqlalchemy import String, Column, Integer, ForeignKey, Date, Sequence, Boolean
from sqlalchemy.orm import relationship

from app.service.sqlalchemy import BaseAlch
from app.api.employee.models.Employee import EmployeeModel


# CREATE TABLE public.users (
# 	id serial4 NOT NULL,
# 	email varchar NOT NULL,
# 	first_name varchar NOT NULL,
# 	last_name varchar NOT NULL,
# 	sur_name varchar NULL,
# 	"password" varchar NOT NULL,
# 	role_id int4 NOT NULL,
# 	description text NULL,
# 	CONSTRAINT users_pk PRIMARY KEY (id),
# 	CONSTRAINT users_un UNIQUE (email)
# );

# CREATE TABLE public.permissions (
# 	id serial4 NOT NULL,
# 	"name" varchar NOT NULL,
# 	"type" varchar NULL,
# 	user_id int4 NOT NULL,
# 	CONSTRAINT permissions_pk PRIMARY KEY (id)
# );

class UserModel(BaseAlch):
    __tablename__ = 'users'
    users_id_seq = Sequence('users_id_seq')
    id = Column(Integer, users_id_seq, server_default=users_id_seq.next_value(), primary_key=True)
    email = Column(String, nullable=False)
    first_name = Column(String, nullable=False)
    last_name = Column(String, nullable=False)
    sur_name = Column(String, nullable=True)
    password = Column(String, nullable=False)
    role_id = Column(Integer, nullable=True)
    description = Column(String, nullable=True)
    is_deleted = Column(Boolean, nullable=False, default=False)
    # permissions = relationship('PermisssionModel')


class PermisssionModel(BaseAlch):
    __tablename__ = 'permissions'
    permissions_id_seq = Sequence('permissions_id_seq')
    id = Column(Integer, permissions_id_seq, server_default=permissions_id_seq.next_value(), primary_key=True)
    name = Column(String, nullable=False)
    type = Column(String, nullable=True)
    user_id = Column(Integer, ForeignKey(UserModel.id), nullable=False)
