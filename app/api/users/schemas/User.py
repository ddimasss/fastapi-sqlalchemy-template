from pydantic import BaseModel, Field
from typing import List, Optional, Any, Literal, Union
from datetime import date


# CREATE TABLE public.users (
# 	id serial4 NOT NULL,
# 	email varchar NOT NULL,
# 	first_name varchar NOT NULL,
# 	last_name varchar NOT NULL,
# 	sur_name varchar NULL,
# 	"password" varchar NOT NULL,
# 	role_id int4 NOT NULL,
# 	description text NULL,
# 	CONSTRAINT users_pk PRIMARY KEY (id),
# 	CONSTRAINT users_un UNIQUE (email)
# );

# CREATE TABLE public.permissions (
# 	id serial4 NOT NULL,
# 	"name" varchar NOT NULL,
# 	"type" varchar NULL,
# 	user_id int4 NOT NULL,
# 	CONSTRAINT permissions_pk PRIMARY KEY (id)
# );
class LoginScheme(BaseModel):
    class Config:
        from_attributes = True
    email: str
    password: str


class PermissionsScheme(BaseModel):
    class Config:
        from_attributes = True

    id: Optional[int] = None
    name: Literal[
                'customers_view',
                'customers_edit',
                'customer_send',
                'users_view',
                'users_edit',
                'settings_edit',
                'settings_view'
            ]
    type: Optional[str]
    user_id: int


class UserScheme(BaseModel):
    class Config:
        from_attributes = True

    id: Optional[int] = None
    email: str
    first_name: str
    last_name: str
    sur_name: Optional[str]
    password: str
    description: Optional[str]
    is_deleted: bool = False
    permissions: Optional[
        List[
            Literal[
                'customers_view',
                'customers_edit',
                'customer_send',
                'users_view',
                'users_edit',
                'settings_edit',
                'settings_view'
            ]
        ]
    ] = []
