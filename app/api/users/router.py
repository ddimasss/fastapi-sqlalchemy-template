from fastapi import APIRouter
from .actions.add import add
from .actions.edit import edit
from .actions.get import get, get_list
from .actions.login import login
from .schemas.User import UserScheme
from typing import List


user_router = APIRouter()


user_router.add_api_route(
    path='/add',
    endpoint=add,
    methods=['POST'],
    tags=['other'],
    response_model=UserScheme
)
user_router.add_api_route(
    path='/edit',
    endpoint=edit,
    methods=['POST'],
    tags=['other'],
    response_model=UserScheme
)
user_router.add_api_route(
    path='/get',
    endpoint=get,
    methods=['GET'],
    tags=['other'],
    response_model=UserScheme
)
user_router.add_api_route(
    path='/get_list',
    endpoint=get_list,
    methods=['GET'],
    tags=['other'],
    response_model=List[UserScheme]
)

user_router.add_api_route(
    path='/login',
    endpoint=login,
    methods=['POST'],
    tags=['other'],
    response_model=UserScheme
)
