from fastapi import Depends
from app.service.sqlalchemy import get_session, get_session_sync
from ..models.Users import UserModel, PermisssionModel
from ..schemas.User import UserScheme
import logging


async def add(
        user_request: UserScheme,
        db=Depends(get_session_sync)
):
    if 'id' in user_request:
        del user_request.id
    db_user = UserModel(
        email=user_request.email.lower(),
        first_name=user_request.first_name,
        last_name=user_request.last_name,
        sur_name=user_request.sur_name,
        password=user_request.password,
        description=user_request.description
    )
    db.add(db_user)
    db.flush()
    for permission in user_request.permissions:
        permission_model = PermisssionModel(
            name=permission,
            user_id=db_user.id
        )
        db.add(permission_model)

    db.commit()
    db.refresh(db_user)
    db_user.permissions = user_request.permissions

    return db_user
