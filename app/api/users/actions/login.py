from fastapi import Depends, HTTPException
from sqlalchemy import and_, func
from app.service.sqlalchemy import get_session_sync
from ..models.Users import UserModel, PermisssionModel
from ..schemas.User import LoginScheme


async def login(
        user: LoginScheme,
        db=Depends(get_session_sync)
):
    obj = db.query(UserModel, func.array_agg(PermisssionModel.name)) \
        .join(PermisssionModel, UserModel.id == PermisssionModel.user_id, isouter=True) \
        .filter(and_(func.lower(UserModel.email) == user.email.lower(), UserModel.password == user.password)) \
        .group_by(UserModel.id) \
        .first()
    db.commit()
    if obj:
        res = obj[0]
        res.permissions = obj[1] if obj[1] != [None] else []

        return res
    else:
        raise HTTPException(status_code=403, detail="Access denied")

