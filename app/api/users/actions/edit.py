from fastapi import Depends
from app.service.sqlalchemy import get_session_sync
from ..models.Users import UserModel, PermisssionModel
from ..schemas.User import UserScheme


async def edit(
        user_request: UserScheme,
        db=Depends(get_session_sync)
):
    obj: UserModel = db.query(UserModel).get(user_request.id)
    obj.email = user_request.email
    obj.description = user_request.description
    obj.first_name = user_request.first_name
    obj.last_name = user_request.last_name
    obj.sur_name = user_request.sur_name
    obj.password = user_request.password
    obj.is_deleted = user_request.is_deleted
    db.merge(obj)
    db.flush()
    addresses = db.query(PermisssionModel).filter(PermisssionModel.user_id == user_request.id)
    addresses.delete()
    db.flush()
    for permission in user_request.permissions:
        permission_model = PermisssionModel(
            name=permission,
            user_id=obj.id
        )
        db.add(permission_model)

    db.commit()
    obj.permissions = user_request.permissions
    return obj
