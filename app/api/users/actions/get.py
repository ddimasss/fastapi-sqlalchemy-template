from fastapi import Depends
from sqlalchemy import func
from sqlalchemy.orm import joinedload

from app.service.sqlalchemy import get_session_sync
from ..models.Users import UserModel, PermisssionModel


async def get(
        position_id: int,
        db=Depends(get_session_sync)
):
    obj = db.query(UserModel, func.array_agg(PermisssionModel.name)) \
        .join(PermisssionModel, UserModel.id == PermisssionModel.user_id, isouter=True) \
        .filter(UserModel.id == position_id)\
        .group_by(UserModel.id) \
        .first()
    db.commit()
    res = obj[0]
    res.permissions = obj[1] if obj[1] != [None] else []
    return res


async def get_list(
        page: int = 0,
        limit: int = 0,
        db=Depends(get_session_sync)
):
    offset = page * limit
    obj = db.query(UserModel, func.array_agg(PermisssionModel.name)) \
        .join(PermisssionModel, UserModel.id == PermisssionModel.user_id, isouter=True) \
        .group_by(UserModel.id) \
        .limit(limit) \
        .offset(offset) \
        .all()
    list_ = []
    for value in obj:
        if value[1] == [None]:
            permissions = []
        else:
            permissions = value[1]
        value[0].permissions = permissions
        list_.append(value[0])

    return list_
