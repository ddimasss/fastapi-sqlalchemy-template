from fastapi import Depends
from app.service.sqlalchemy import get_session_sync
from ..models.Settings import SettingsModel
from ..schemas.Settings import SettingsScheme


async def edit(
        settings_request: SettingsScheme,
        db=Depends(get_session_sync)
):
    obj = db.query(SettingsModel).get(settings_request.id)
    obj.setting_name = settings_request.setting_name
    obj.setting_value = settings_request.setting_value
    db.commit()

    return obj
