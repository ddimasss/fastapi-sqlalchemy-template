from pydantic import (
    BaseSettings
)

ENV_PREFIX = 'REP_CUST_CAT_'


class CustomerCategorySettings(BaseSettings):

    class Config:
        env_prefix = ENV_PREFIX


customer_category_settings = CustomerCategorySettings()