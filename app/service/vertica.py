from vertica_python import (
    connect as vertica_connect,
    Connection as VerticaConnection
)
from vertica_python.vertica.cursor import (
    Cursor as VerticaCursor
)
import vertica_python
from app.settings import settings


def _connect() -> VerticaConnection:
    return vertica_connect(**settings.VERTICA_SETTINGS.dict())


_conn: VerticaConnection = _connect()


def get_vertica_cursor() -> VerticaCursor:
    global _conn
    try:
        return _conn.cursor()
    except vertica_python.errors.ConnectionError:
        _conn.close()
        _conn = _connect()
        return _conn.cursor()
