from sqlalchemy.orm import sessionmaker, Session
from sqlalchemy import create_engine
from ..settings import settings
from typing import Generator
from sqlalchemy.ext.asyncio import (
    create_async_engine,
    AsyncSession
)
from sqlalchemy.ext.declarative import declarative_base


engine = create_async_engine(settings.POSTGRES_URL, pool_pre_ping=True, echo=True)
Pg_session = sessionmaker(autocommit=False, autoflush=False, bind=engine,  future=True, class_=AsyncSession)

engine_sync = create_engine(settings.POSTGRES_SYNC_URL, pool_pre_ping=True, echo=True)
pg_session_sync = sessionmaker(autocommit=False, autoflush=False, bind=engine_sync,  future=True)

#  service_engine = create_async_engine(settings.POSTGRES_SETTINGS_URL, pool_pre_ping=True, echo=True)
#  Pg_service_session = sessionmaker(autocommit=False, autoflush=False, bind=service_engine, future=True, class_=AsyncSession)
#  test_service_engine = create_engine(settings.POSTGRES_SETTINGS_TEST_URL, pool_pre_ping=True, echo=True)
#  Pg_service_test_session = sessionmaker(autocommit=False, autoflush=False, bind=test_service_engine, future=True)


async def get_session() -> Generator:
    try:
        db: AsyncSession = Pg_session()
        yield db
    finally:
        await db.close()


def get_session_sync() -> Generator:
    try:
        db: Session = pg_session_sync()
        yield db
    finally:
        db.close()


BaseAlch = declarative_base()
BaseAlch.__table_args__ = {'schema': 'public'}


# async def get_service_session() -> Generator:
#     try:
#         service_db: AsyncSession = Pg_service_session()
#         yield service_db
#     finally:
#         await service_db.close()


# def get_service_test_session() -> Generator:
#     try:
#         service_test_db: Session = Pg_service_test_session()
#         yield service_test_db
#     finally:
#         service_test_db.close()
