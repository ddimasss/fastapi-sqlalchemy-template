import fastapi
from app.service.rabbitmq import sync_consumer
from starlette.middleware.cors import CORSMiddleware
from app.api import api_router
import logging
import threading

t: threading.Thread
logging.basicConfig(filename='tmp/app.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')

app = fastapi.FastAPI()
app.add_middleware(
    CORSMiddleware, 
    allow_origins=['*'], 
    allow_methods=["*"], 
    allow_credentials=True, 
    allow_headers=["*"]
)


@app.on_event('startup')
async def startup():
    pass
    # global t
    # t = threading.Thread(target=sync_consumer)
    # t.start()


app.include_router(api_router, prefix="/fin_api")


@app.on_event('shutdown')
async def on_shutdown():
    pass
    # t.join()
