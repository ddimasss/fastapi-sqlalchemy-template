FROM python:3.9

COPY requirements.txt /python-api/
WORKDIR /python-api

RUN apt update && apt install libreoffice-core -y
RUN pip install --upgrade pip && pip install -r requirements.txt

MKDIR /doc-manager-api
WORKDIR /doc-manager-api
CMD python run.py
