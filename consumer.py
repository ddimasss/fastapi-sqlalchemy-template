import json
import datetime
import pika
import logging


logging.basicConfig(level=logging.INFO, filename='/opt/app/consumer.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')


def sync_consumer():
    credentials = pika.PlainCredentials('lim', 'Serial111')
    connection = pika.BlockingConnection(pika.ConnectionParameters('188.168.35.10',  credentials=credentials))
    channel = connection.channel()

    channel.queue_declare(queue='hello')

    def callback(ch, method, properties, body):
        from app.generator.main import generate_any
        a = json.loads(body.decode())
        d = datetime.datetime.strptime(a['date'], "%Y-%m-%d").date()
        logging.info('Start generate')
        logging.info(str(a))
        try:
            i = generate_any(a['customer_id'], d, a['ids'], a['user_id'])
        except Exception as e:
            logging.error(e)
        else:
            logging.info(i)

    channel.basic_consume(queue='hello', on_message_callback=callback, auto_ack=True)
    logging.debug(' [*] Waiting for messages. To exit press CTRL+C')
    channel.start_consuming()


if __name__ == '__main__':
    logging.info('Starting consumer')
    sync_consumer()
